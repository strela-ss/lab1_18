from django.http import HttpResponseRedirect
from django.utils import translation
from django.utils.translation import check_for_language

from lab1_18 import settings


def set_language(request, lang):
    response = HttpResponseRedirect("/")
    if lang and check_for_language(lang):
        translation.activate(lang)
        response.set_cookie(settings.LANGUAGE_COOKIE_NAME, lang)
        request.session['_language'] = lang
    return response
