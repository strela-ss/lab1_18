1. Install [Python 2.7.12](https://www.python.org/downloads/)
2. Install MySQL server, workbench, Python connector from [here](https://dev.mysql.com/downloads/windows/installer/5.7.html).
3. Set parameters for MySQL server:
    * login: root
    * password: root
    * port: 3306
    * database name: lab1_18
4. Install requirements.conf



    `pip install -r requirements.conf`



    * If you will have troubles with MySQL-python install it with .whl file with command


    * `pip install MySQL_python-1.2.5-cp27-none-win32.whl`

5. Run migrations with command:



    `python manage.py migrate`

6. Create superuser with command:



    `python manage.py createsuperuser`

7. Run server:



    `python manage.py runserver`

8. Open in browser:


    [localhost:8000](localhost:8000)

9. To access admin interface use this [link](localhost:8000/admin) and superuser *login* and *password*.