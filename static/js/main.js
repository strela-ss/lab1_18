function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i]);
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}
$.ajaxSetup({
  beforeSend: function (xhr, settings) {
    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
  }
});

var auth = $("#auth");

function login_module() {
  auth.empty();
  $("#order").empty();
  $.get("/forms/", {}, function (data) {
    auth.append(
      '<h4>' + gettext("Registration") + '</h4> <form id="reg_form">'+data.reg_form+'<button type="button" id="registration">'+gettext("Registrate")+'</button> </form> <br/> <h4>'+gettext("Login")+'</h4> <form id="login_form">'+data.login_form+'<button type="button" id="login">'+gettext("Login")+'</button> </form>'
    )
  });
}
function logout_module(name) {
  auth.empty();
  auth.append(
    '<h2>' + gettext('Welcome ') + name + '<button id="logout">Logout</button> </h2>'
  )
}
function get_order() {
  $.post("/rest/order/", {}, function (data) {
    var order = $("#order"),
      tours = data.tours;
    order.empty();
    order.append("<h3>" + gettext("Cart: ") + "</h3> <ul id='items'></ul>");
    for (var i = 0; i < tours.length; i++) {
      var tour = tours[i];
      $("#items").append("<li>" + tour.name + " " + tour.cost + "<button id='" + tour.id + "-rm' class='cart-rm'>" + gettext("Remove") + "</button></li>")
    }
    order.append(gettext("Total: ") + data.sum);
    order.append("<br><button id='confirm'>" + gettext("Confirm") + "</button>")
  }).error(function () {
    console.log("You are not logged");
  });
}

$(document).ready(function () {
  var body = $("body");
  get_order();

  $.get("/rest/tours/", {}, function (data) {
    var tours = $("#tours tbody");
    for (var i = 0; i < data.length; i++) {
      var tour = data[i];
      tours.append(
        "<tr>" +
        "<td>" + tour.id + "</td>" +
        "<td>" + tour.name + "</td>" +
        "<td>" + tour.tour_format + "</td>" +
        "<td>" + tour.cost + "</td>" +
        "<td><button id='" + tour.id + "-button' class='order' type='button'>" + gettext("Order") + "</button> </td>" +
        "</tr>"
      )
    }
  });

  body.on("click", "#logout", function () {
    $.post('/logout/', {}, function (data) {
      console.log(data);
      login_module();
    })
  });

  body.on("click", "#login", function () {
    var self = $(this);
    $.post('/login/', self.closest("form").serialize(), function (data) {
      console.log(data);
      logout_module(data.name);
      get_order();
    }).error(function (data) {
      $("#login_form .error").remove();
      $("#login_form").append(
        "<i class='error'>" + gettext("*User with specified data isn't found") + "</i>"
      )
    });
    return false;
  });

  body.on("click", "#registration", function () {
    var self = $(this);
    $.post('/register', self.closest("form").serialize(), function (data) {
      console.log(data);

    }).error(function (data) {
      $("#reg_form .error").each(function () {
        $(this).remove();
      });
      data = data.responseJSON;
      console.log(data);
      $.each(data, function (key, val) {
        for (var i = 0; i < val.length; i++) {
          $("#reg_form input[name='" + key + "']").parent().append(
            "<i class='error'>" + val[i] + "</i>"
          )
        }
      });
    });
    return false;
  });

  body.on("click", ".order", function () {
    var self = $(this),
      tour_id = self.attr("id").split("-")[0];
    $.post("/tocart/", {id: tour_id}, function (data) {
      get_order();
    });
  });

  body.on("click", ".cart-rm", function () {
    var self = $(this),
      tour_id = self.attr("id").split("-")[0];
    $.post("/rmcart/", {id: tour_id}, function (data) {
      get_order();
    });
  });

  body.on("click", "#confirm", function () {
    var self = $(this);
    $.post("/confirm/", {}, function () {
      get_order();
    }).error(function () {
      alert(gettext("Your cart is empty"));
    });
  });
});