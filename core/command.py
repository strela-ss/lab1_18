class Command:
    def execute(self):
        pass


class AddToBaseCommand(Command):
    def __init__(self, table, user, data):
        self.table = table
        self.user = user
        self.data = data

    def execute(self):
        self.table.objects.get(user=self.user).tours.add(self.data)


class DeleteFromBaseCommand(Command):
    def __init__(self, table, user, data):
        self.table = table
        self.user = user
        self.data = data

    def execute(self):
        self.table.objects.get(user=self.user).tours.remove(self.data)


class Macro:
    def __init__(self):
        self.commands = []

    def add(self, command):
        self.commands.append(command)

    def run(self):
        for c in self.commands:
            c.execute()
