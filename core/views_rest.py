from rest_framework import viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models import Tour, Order
from core.serializers import TourSerializer, OrderSerializer, PremiumOrderSerializer, PremiumTourSerializer
from core.utils import get_order


class TourViewSet(viewsets.ModelViewSet):
    queryset = Tour.objects.all()
    serializer_class = TourSerializer
    ordering = ('name',)

    def list(self, request, *args, **kwargs):
        if request.user.is_authenticated() and request.user.premium:
            self.serializer_class = PremiumTourSerializer
        return super(TourViewSet, self).list(request, *args, **kwargs)


class OrderViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    ordering = ('created',)
    permission_classes = [IsAdminUser]


class OrderView(APIView):

    def post(self, request, format=None):
        if request.user.is_authenticated():
            user = request.user
        else:
            raise PermissionDenied()
        order = get_order(user)
        if user.premium:
            serializer = PremiumOrderSerializer(order, context={'request': request})
        else:
            serializer = OrderSerializer(order, context={'request': request})
        return Response(serializer.data)
