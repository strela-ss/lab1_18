import json

from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.utils import translation
from django.utils.translation import ugettext as _
from django.views import View
from django.views.generic import TemplateView

from core.forms import *
from core.models import Tour, Order
from core.utils import get_order


class Index(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data()
        context.update({
            'tours': Tour.objects.all(),
            'reg_form': RegistrationForm,
            'login_form': LoginForm,
        })
        return context


class GetForms(View):
    def get(self, request, *args, **kwargs):
        return JsonResponse({
            'reg_form': RegistrationForm().as_p(),
            'login_form': LoginForm().as_p(),
        })


class LogoutView(View):
    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            logout(request)
            response = HttpResponse("ok")
            response.delete_cookie('sessionid')
        else:
            response = HttpResponse("fail")
            response.status_code = 400
        return response


class LoginView(View):
    def post(self, request):
        form = LoginForm(request.POST)
        if form.is_valid():
            cleaned = form.cleaned_data
            name = cleaned['name']
            password = cleaned['password']
            user = authenticate(username=name, password=password)
            if user is not None:
                login(request, user)
                return JsonResponse({
                    'name': user.name,
                })
            else:
                response = HttpResponse(_("Invalid name or/and password"))
                response.status_code = 403
                return response
        response = JsonResponse(form.errors)
        response.status_code = 400
        return response


class RegisterView(View):
    def post(self, request, *args, **kwargs):
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse('success')
        response = JsonResponse(form.errors)
        response.status_code = 400
        return response


class ToCartView(View):
    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated() or 'id' not in request.POST:
            raise PermissionDenied()
        user = request.user
        tour_id = request.POST['id']
        order = get_order(user)
        order.tours.add(get_object_or_404(Tour, id=tour_id))
        return HttpResponse('ok')


class RemoveFromCart(View):
    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated() or 'id' not in request.POST:
            raise PermissionDenied()
        user = request.user
        tour_id = request.POST['id']
        order = get_order(user)
        order.tours.remove(get_object_or_404(Tour, id=tour_id))
        return HttpResponse('ok')


class ConfirmView(View):
    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            raise PermissionDenied()
        user = request.user
        order = get_order(user)
        if order.tours.count() > 0:
            order.status = True
            order.save()
            return HttpResponse('ok')
        else:
            response = HttpResponse('fail')
            response.status_code = 400
            return response


def info(request):
    context = {
        'tours': Tour.objects.all(),
        'users': ExtUser.objects.all(),
        'orders': Order.objects.all(),
    }
    return render(request, 'info.html', context)


class UpdateView(View):
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        for tour in data['data']:
            t = get_object_or_404(Tour, id=tour['id'])
            t.discount = tour['discount']
            t.hot = True if 'hot' in tour else False
            t.save()
        return HttpResponse('ok')
