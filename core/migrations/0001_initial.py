# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-03 17:29
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ExtUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('name', models.CharField(max_length=32, verbose_name='name')),
                ('premium', models.BooleanField(default=False)),
                ('is_admin', models.BooleanField(default=False, verbose_name='is admin')),
                ('is_manager', models.BooleanField(default=False, verbose_name='is manager')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Order',
                'verbose_name_plural': 'Orders',
            },
        ),
        migrations.CreateModel(
            name='Tour',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('format', models.CharField(choices=[('r', 'Rest'), ('e', 'Ekscursion'), ('s', 'Shopping')], max_length=1)),
                ('hot', models.BooleanField(default=False)),
                ('discount', models.FloatField(default=0)),
                ('cost', models.FloatField()),
            ],
            options={
                'verbose_name': 'Tour',
                'verbose_name_plural': 'Tours',
            },
        ),
        migrations.AddField(
            model_name='order',
            name='tours',
            field=models.ManyToManyField(blank=True, to='core.Tour'),
        ),
        migrations.AddField(
            model_name='order',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.ExtUser'),
        ),
    ]
