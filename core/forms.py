from django import forms
from django.utils.translation import ugettext_lazy as _

from core.models import ExtUser


class RegistrationForm(forms.ModelForm):
    def clean_name(self):
        data = self.cleaned_data['name']
        if len(data) < 3:
            raise forms.ValidationError(_("Name can't be less than 3 symbols"))
        return data

    name = forms.CharField(
        label=_("Name"),
        required=True,
        widget=forms.TextInput(attrs={'placeholder': _("Name")}),
    )
    _password = forms.CharField(
        label=_('Password'),
        required=True,
        widget=forms.PasswordInput(attrs={'placeholder': _("Password")}),
    )
    confirmation = forms.CharField(
        label=_('Confirmation'),
        required=True,
        widget=forms.PasswordInput(attrs={'placeholder': _("Confirmation")}),
    )

    def clean_confirmation(self):
        password = self.cleaned_data['_password']
        confirmation = self.cleaned_data['confirmation']
        if password == confirmation:
            return password
        else:
            raise forms.ValidationError(_("Passwords do not match"))

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['_password'])
        if commit:
            user.save()
        return user

    class Meta:
        model = ExtUser
        fields = (
            'name',
            '_password',
            'confirmation'
        )


class LoginForm(forms.Form):
    name = forms.CharField(
        label=_('Name'),
        required=True,
        widget=forms.TextInput(attrs={'placeholder': _('Name')}),
    )
    password = forms.CharField(
        label=_('Password'),
        required=True,
        widget=forms.PasswordInput(attrs={'placeholder': _("Password")}),
    )
