from django.contrib import admin
from django.contrib.auth.models import Group

from core.models import Tour, Order, ExtUser


class ExtUserAdmin(admin.ModelAdmin):
    fields = ('name', 'premium', 'is_manager')
    list_display = ('name', 'premium', 'is_manager')

    def is_manager(self, obj):
        return obj.is_staff

    is_manager.boolean = True
    is_manager.short_description = "Is manager"


class TourAdmin(admin.ModelAdmin):
    list_display = ('name', 'hot', 'cost', 'discount', 'cost_discount')
    list_filter = ('format', 'hot')


admin.site.register(Tour, TourAdmin)
admin.site.register(Order)
admin.site.register(ExtUser, ExtUserAdmin)
admin.site.unregister(Group)
