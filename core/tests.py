import json

from django.test import TestCase, Client

from core.models import Tour, ExtUser


class ToursTestCase(TestCase):
    def setUp(self):
        Tour.objects.create(name="First tour", format="r", cost=13750)
        Tour.objects.create(name="Second tour", format="s", cost=750)

    def test(self):
        c = Client()
        response = c.get('/rest/tours/', {})
        content = json.loads(response.content)
        self.assertEqual(len(content), 2)


class RegisterTest(TestCase):
    def test(self):
        c = Client()
        response = c.post('/register/', {'name': 'test', '_password': '1234qwer', 'confirmation': '1234qwer'})
        self.assertEqual(response.status_code, 200)


class LoginTest(TestCase):
    def test(self):
        c = Client()
        response = c.post('/register/', {'name': 'test', '_password': '1234qwer', 'confirmation': '1234qwer'})
        self.assertEqual(response.status_code, 200)
        response = c.post('/login/', {'name': 'test', 'password': '1234qwer'})
        self.assertEqual(response.status_code, 200)


class LoginTest2(TestCase):
    def setUp(self):
        user = ExtUser(name='test')
        user.set_password('1234qwer')
        user.save()

    def test(self):
        c = Client()
        response = c.post('/login/', {'name': 'test', 'password': '1234qwer'})
        self.assertEqual(response.status_code, 200)
