# coding=utf-8
from __future__ import unicode_literals

from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.db import models
from django.utils.translation import ugettext_lazy as _

"""
18.	Система Турагентство. Заказчик выбирает и оплачивает Тур (отдых, экскурсия, шоппинг).
Турагент определяет тур как «горящий», размеры скидок постоянным клиентам.
"""

TOUR_FORMATS = (("r", _("Rest")), ("e", _("Excursion")), ("s", _("Shopping")))


class MyUserManager(BaseUserManager):
    def create_user(self, name, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """

        user = self.model(
            name=name,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, name, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            name=name,
            password=password,
        )
        user.is_admin = True
        user.is_manager = True
        user.save(using=self._db)
        return user


class ExtUser(AbstractBaseUser):
    name = models.CharField(
        max_length=32,
        null=False,
        blank=False,
        unique=True,
        verbose_name=_('name')
    )
    premium = models.BooleanField(default=False)
    is_admin = models.BooleanField(
        default=False,
        verbose_name=_('is admin'),
    )
    is_manager = models.BooleanField(
        default=False,
        verbose_name=_('is manager'),
    )

    objects = MyUserManager()

    USERNAME_FIELD = 'name'

    def get_short_name(self):
        return self.name

    def get_full_name(self):
        return self.name

    def name_display(self):
        return self.name

    def __unicode__(self):
        return self.name

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_superuser(self):
        return self.is_admin

    @property
    def is_staff(self):
        return self.is_manager

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')


class Tour(models.Model):
    name = models.CharField(max_length=50)
    format = models.CharField(choices=TOUR_FORMATS, max_length=1)
    hot = models.BooleanField(default=False)
    discount = models.FloatField(default=0)
    cost = models.FloatField()

    @property
    def cost_discount(self):
        return self.cost - (self.discount / 100) * self.cost

    def __unicode__(self):
        return "[%s] %s" % (self.get_format_display(), self.name)

    class Meta:
        verbose_name = _("Tour")
        verbose_name_plural = _("Tours")


class Order(models.Model):
    user = models.ForeignKey(ExtUser)
    tours = models.ManyToManyField(Tour, blank=True)
    status = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    @property
    def sum(self):
        s = 0
        premium = self.user.premium
        for tour in self.tours.all():
            s += tour.cost if premium else tour.cost_discount
        return s

    class Meta:
        verbose_name = _("Order")
        verbose_name_plural = _("Orders")
