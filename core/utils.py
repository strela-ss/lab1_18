from core.models import Order


def get_order(user):
    order = Order.objects.filter(user=user, status=False)
    if not order:
        order = Order(user=user)
        order.save()
    else:
        order = order[0]
    return order
