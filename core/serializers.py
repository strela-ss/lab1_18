from rest_framework import serializers

from core.models import Tour, Order


class TourSerializer(serializers.ModelSerializer):
    tour_format = serializers.CharField(source='get_format_display', read_only=True)

    class Meta:
        model = Tour
        fields = ('id', 'name', 'tour_format', 'hot', 'discount', 'cost')


class PremiumTourSerializer(serializers.ModelSerializer):
    tour_format = serializers.CharField(source='get_format_display', read_only=True)
    cost = serializers.FloatField(source="cost_discount")

    class Meta:
        model = Tour
        fields = ('id', 'name', 'tour_format', 'hot', 'discount', 'cost')


class OrderSerializer(serializers.HyperlinkedModelSerializer):
    tours = TourSerializer(many=True, read_only=True)
    user = serializers.CharField(source='user.name')

    class Meta:
        model = Order
        fields = ("user", 'tours', 'status', 'sum')
        depth = 1


class PremiumOrderSerializer(serializers.HyperlinkedModelSerializer):
    tours = PremiumTourSerializer(many=True, read_only=True)
    user = serializers.CharField(source='user.name')

    class Meta:
        model = Order
        fields = ("user", 'tours', 'status', 'sum')
        depth = 1
